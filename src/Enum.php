<?php
namespace bdhert\PhpStruct;

/**
 * 枚举结构体
 * Class Enum
 * @package bdhert\PhpStruct
 */
abstract class Enum extends Standard {
    public $value = NULL;

    /**
     * 初始化
     */
    protected function init() {
        if ($this->value && !in_array($this->value, $this->getRef()->getConstants())) $this->value = NULL;
    }

    /**
     * 判断值是否存在
     * @return bool
     */
    public function exists(): bool {
        if (is_null($this->value)) return false;

        if (!in_array($this->value, $this->getRef()->getConstants())) {
            $this->value = NULL; return false;
        }

        return true;
    }
}