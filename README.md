# php-struct

#### 介绍
使用PHP实现结构体，结构体可以代替简单的类、维护关键属性、描述业务对象。


#### 使用场景与优点
1.  在优化和抽象业务代码的时候，经常会有比较复杂但又不需要着重抽象为service的部分，可以抽象为一个简单类或者一个结构体。以此增加代码的可读性；
2.  维护一个结构体，可以以维护类的方式扩展和修改业务逻辑，这种方式比面向过程更简洁、更灵活；
3.  由于结构体的对属性的扩展与描述有很强的灵活性，使得其在参数透传、复建等场景非常实用。


#### 安装

1.  composer require bdhert/php-struct
2.  开发版：composer require bdhert/php-struct:"dev-master"
3.  基础版：composer require bdhert/php-struct:"^0.1"


#### 配置

```php
<?php
class Paging extends bdhert\PhpStruct\Standard {
    public int    $page   = 1;
    public int    $limit  = 20;
    public int    $offset = 0;
    public int    $total  = 0;
    public ?array $list   = NULL;

    /**
     * 起跳参数初始化
     */
    protected function init(): void {
        $this->offset = ($this->page - 1) * $this->limit;
    }
}

$paging = Paging::build($request->all());
$paging->total = $query->count();
$paging->list  = $query->get()->toArray();
return $paging->toArray();
```
